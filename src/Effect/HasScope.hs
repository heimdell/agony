
module Effect.HasScope where

import Polysemy        (Sem, Member, makeSem, interpretH, pureT, runT, raise)
import Polysemy.Reader (Reader, ask, local)

import Effect.Builtins (Builtins, builtin)
import Program         (Program (Nop), Frame, Scope)

import qualified Name
import qualified Scope

data HasScope m a where
  Find :: Name.T -> HasScope m Program
  With :: Frame -> m a -> HasScope m a

makeSem ''HasScope

runScope
  :: Member (Reader Scope) r
  => Member  Builtins        r
  => Sem (HasScope : r) a
  -> Sem r a
runScope = interpretH \case
  Find name -> do
    ctx <- ask
    case Scope.find name ctx of
      Just it -> do
        pureT it

      Nothing -> do
        builtin name
        pureT Nop

  With frame action -> do
    action' <- runT action
    local (Scope.add frame) do
      raise do
        runScope action'

