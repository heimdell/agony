
module Effect.Access where

import Polysemy       (Sem, Member, makeSem, interpret)
import Polysemy.Error (Error, throw)

import Errors         (NotFound(..), NotAnObject (..))
import Program        (Program)
import Value          (Value (Box))

import qualified Scope
import qualified Name

data Access (m :: * -> *) a where
  Access :: Value -> Name.T -> Access m Program

makeSem ''Access

runAccess
  :: Member (Error NotFound)    r
  => Member (Error NotAnObject) r
  => Sem (Access : r) a
  -> Sem r a
runAccess = interpret \case
  Access val name -> do
    case val of
      Box (Scope.single -> ctx) -> do
        case Scope.find name ctx of
          Just it -> return it
          Nothing -> throw $ NotFound name (Scope.keys ctx)

      _ -> do
        throw NotAnObject

