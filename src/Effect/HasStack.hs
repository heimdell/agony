
module Effect.HasStack where

import Polysemy       (Sem, Member, makeSem, interpretH, pureT, runT, raise)
import Polysemy.Error (Error, throw)
import Polysemy.State (State, get, put, modify)

import Errors         (StackUnderflow (..))
import Value          (Value, Stack)

import qualified Stack

data HasStack m a where
  Push       :: Value -> HasStack m ()
  Pop        :: HasStack m Value
  ParallelTo :: m () -> m () -> HasStack m ()

makeSem ''HasStack

runStack
  :: Member (State Stack) r
  => Member (Error StackUnderflow) r
  => Member (State Int) r
  => Sem (HasStack : r) a
  -> Sem r a
runStack = interpretH \case
  Push v -> do
    i <- get @Int
    modify @Int (+ 1)
    modify (Stack.push i v)
    pureT ()

  Pop -> do
    stack <- get
    case Stack.pop stack of
      Just (top, rest) -> do
        put rest
        pureT top

      _ -> do
        throw StackUnderflow

  ParallelTo ma mb -> do
    old <- get
    ma' <- runT ma
    _   <- raise $ runStack ma'
    new <- get
    let (delta, new1) = Stack.diff old new
    put new1
    mb' <- runT mb
    _   <- raise $ runStack mb'
    i <- get @Int
    modify @Int (+ 1)
    modify (Stack.append i delta)
    pureT ()
