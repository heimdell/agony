
module Effect.Builtins where

import Polysemy        (Sem, Member, makeSem, interpret)

import Effect.HasStack (HasStack)

import qualified Name

data Builtins (m :: * -> *) a where
  Builtin :: Name.T -> Builtins m ()

makeSem ''Builtins

runBuiltins
  :: Member HasStack r
  => (Name.T -> Sem r ())
  -> Sem (Builtins : r) a
  -> Sem r a
runBuiltins stdlib = interpret \case
  Builtin name -> stdlib name
