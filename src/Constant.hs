
module Constant where

data Constant
  = Num Double
  | Str String
  deriving stock Show
