
module Program where

import Data.String

import GHC.OverloadedLabels (IsLabel (..))
import GHC.TypeLits (KnownSymbol, Symbol)

import Constant
import qualified Name
import qualified Scope

data Program
  = Seq Program  Program
  | Par Program  Program
  | Nop
  | Let Frame    Program
  | Use [Name.T] Program
  | Lam [Name.T] Program
  | Call  Name.T
  | Quote Program
  | Const Constant
  | Obj   Frame
  | Prism String
  deriving stock Show

type Frame = Scope.Frame Name.T  Program
type Scope = Scope.T     Name.T  Program

instance KnownSymbol (label :: Symbol) => IsLabel label Program where
  fromLabel = Call (fromLabel @label)

instance IsString Program where
  fromString = Const . Str

instance Num Program where
  fromInteger = Const . Num . fromInteger

infixr 2 `Seq`
infixr 1 `Par`