
module Value where

import Program
import Constant
import qualified Stack

data Value
  = Val Constant
  | Box Frame
  | Fun Program
  | Prizm String
  deriving stock Show

type Stack = Stack.T Value

uneval :: Value -> Program
uneval = \case
  Box   b -> Seq (Obj   b) Nop
  Val   c -> Seq (Const c) Nop
  Prizm p -> Seq (Prism p) Nop
  Fun   p -> p
