
module Name where

import Data.Default (def)
import Data.Function (on)
import Data.String.Interpolate (i)

import GHC.OverloadedLabels (IsLabel (..))
import GHC.TypeLits (KnownSymbol, symbolVal', Symbol)
import GHC.Exts (Proxy#, proxy#)

import qualified Position

data T = Name
  { raw :: String
  , pos :: Position.T
  , idx :: Int
  }

instance Show T where
  show Name { raw, idx = 0 } =      raw
  show Name { raw, idx }     = [i|#{raw}@#{idx}|]

instance KnownSymbol (label :: Symbol) => IsLabel label T where
  fromLabel = Name (symbolVal' (proxy# :: Proxy# label)) def 0

instance Eq T where
  (==) = (==) `on` show

instance Ord T where
  compare = compare `on` show