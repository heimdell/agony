
module Errors where

import qualified Name

data NotFound       = NotFound Name.T [Name.T] deriving stock Show
data NotAnObject    = NotAnObject              deriving stock Show
data NotANumber     = NotANumber               deriving stock Show
data StackUnderflow = StackUnderflow           deriving stock Show
data Undefined      = Undefined Name.T         deriving stock Show

