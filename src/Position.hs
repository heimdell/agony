
module Position where

import Data.Default (Default (def))
import Data.String.Interpolate (i)

data T = Position
  { line :: Int
  , col  :: Int
  , off  :: Int
  , src  :: String
  }

instance Show T where
  show (Position line col _ src) = [i|#{src}:#{line}:#{col}|]

instance Default T where
  def = Position (-1) (-1) (-1) "-"