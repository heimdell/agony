
module Eval where

import Data.Traversable (for)

import Polysemy         (Sem, Member)

import Effect.Access    (Access, access)
import Effect.HasScope  (HasScope, find, with)
import Effect.HasStack  (HasStack, push, pop, parallelTo)
import Program          (Program (..), Frame)
import Value            (Value (..), uneval)

import qualified Scope
import qualified Name

type VM r =
  ( Member Access   r
  , Member HasScope r
  , Member HasStack r
  )

evaluate :: VM r => Program -> Sem r ()
evaluate = \case
  Seq a b -> evaluate a  >>          evaluate b
  Par a b -> evaluate a `parallelTo` evaluate b
  Let f b -> letrec   f `letBind`    evaluate b
  Use n b -> n          `use`        evaluate b
  Lam n b -> n          `lambda`     evaluate b
  Call  n -> call n
  Quote p -> push (Fun p)
  Const c -> push (Val c)
  Obj   f -> push (Box f)
  Prism p -> push (Prizm p)
  Nop     -> return ()

lambda :: VM r => [Name.T] -> Sem r () -> Sem r ()
lambda names prog = do
  pairs <- for names \name -> do
    val <- pop
    return (name, uneval val)

  with (Scope.fromAssoc pairs) do
    prog

letBind :: VM r => Frame -> Sem r () -> Sem r ()
letBind frame run =
  with frame do
    run

use :: VM r => [Name.T] -> Sem r () -> Sem r ()
use names prog = do
  val <- pop
  pairs <- for names \name -> do
    method <- access val name
    return (name, method)

  with (Scope.fromAssoc pairs) do
    prog

call :: VM r => Name.T -> Sem r ()
call name = do
  it <- find name
  evaluate it

letrec :: Frame -> Frame
letrec frame = Scope.map (Let frame) frame

assocNames :: Frame -> String
assocNames = ("{" ++) . (++ "}") . unwords . map (show . fst) . Scope.toAssoc
