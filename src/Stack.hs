
module Stack
  ( T
  , empty
  , depth
  , push
  , pop
  , diff
  , append
  , fromList
  , toList
  ) where

import Control.Arrow (first)

data T a
  = Push
    { sDepth :: Int
    , _sElem :: a
    , _sRest :: T a
    }
  | Empty

empty :: T a
empty = Empty

depth :: T a -> Int
depth = \case
  Push { sDepth } -> sDepth
  Empty           -> 0

push :: Int -> a -> T a -> T a
push i a s = Push i a s

pop :: T a -> Maybe (a, T a)
pop = \case
  Push _ a s -> Just (a, s)
  Empty      -> Nothing

diff :: T a -> T a -> ([a], T a)
diff old = go
  where
    go :: T a -> ([a], T a)
    go (Push d a rest) | d > depth old = first (a :) (go rest)
    go  other                          = ([], other)

append :: Int -> [a] -> T a -> T a
append i = flip (foldr (push i))

instance Show a => Show (T a) where
  show = show . toList

fromList :: [a] -> T a
fromList list = append 0 list Empty

toList :: T a -> [a]
toList = fst . diff Empty
