
module Scope where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Monoid (First (..))

newtype T k v = Scope
  { unScope :: [Frame k v] }
  deriving newtype Show

newtype Frame k v = Frame
  { unFrame :: Map k v }
  deriving newtype Show

empty :: T k v
empty = Scope []

find :: Ord k => k -> T k v -> Maybe v
find k = getFirst . foldMap (First . Map.lookup k) . fmap unFrame . unScope

add :: Frame k v -> T k v -> T k v
add frame (Scope frames) = Scope (frame : frames)

fromAssoc :: Ord k => [(k, v)] -> Frame k v
fromAssoc = Frame . Map.fromList

toAssoc :: Frame k v -> [(k, v)]
toAssoc = Map.toList . unFrame

single :: Frame k v -> T k v
single = Scope . pure

keys :: T k v -> [k]
keys = foldMap (Map.keys . unFrame) . unScope

map :: (v -> v) -> Frame k v -> Frame k v
map f = Frame . Map.map f . unFrame