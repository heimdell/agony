
module Forth where

import Control.Exception hiding (Underflow, evaluate, assert)
import Control.Monad
import Control.Monad.State

import Data.Char
import Data.Map (Map)
import qualified Data.Map as Map

import Lens.Micro.Platform

data Machine = Machine
  { _dict  :: Dictionary
  , _stack :: Stack
  , _depth :: Int
  , _tape  :: String
  }

instance Show Machine where
  show (Machine d s i t) =
    "{ " ++ unwords (map fst d) ++ " }\n" ++
    "level: " ++ show i ++ "\n" ++
    "[ " ++ unwords (map show s) ++ " ]\n" ++
    "> " ++ t

type Dictionary = [(Name, Entry)]

data Entry = Entry Impl Bool
  deriving stock Show

type Name = String

data Impl
  = Native Name Function
  | Threaded [Impl]

instance Semigroup Impl where
  Threaded az <> Threaded bz = Threaded (az <> bz)
  Threaded az <> b = Threaded (az <> [b])
  a <> Threaded bz = Threaded ([a] <> bz)
  a <> b = Threaded [a, b]

type Function = StateT Machine IO ()

type Stack = [Value]

data Value
  = Bool Bool
  | Float Float
  | Num Int
  | Str String
  | Fun Impl
  | Obj (Map String Value)
  deriving stock Show

instance Show Impl where
  show (Native n _) = n
  show (Threaded l) = show l

makeLenses ''Machine

runImpl :: Impl -> Function
runImpl (Native _ f) = f
runImpl (Threaded impls) = sequence_ $ map runImpl impls

class Show x => ToValue x where
  toValue :: x -> Value

instance ToValue Value where
  toValue = id

instance ToValue Bool where
  toValue = Bool

instance ToValue Int where
  toValue = Num

instance ToValue Float where
  toValue = Float

instance ToValue String where
  toValue = Str

instance ToValue Impl where
  toValue = Fun

instance ToValue (Map String Value) where
  toValue = Obj

class FromValue x where
  fromValue :: Value -> IO x

data Expected = Expected String
  deriving stock Show
  deriving anyclass Exception

instance FromValue Value where
  fromValue = return

instance FromValue Bool where
  fromValue (Bool n) = return n
  fromValue  _       = throwIO $ Expected "boolean"

instance FromValue Int where
  fromValue (Num n) = return n
  fromValue  _      = throwIO $ Expected "fixed number"

instance FromValue Float where
  fromValue (Float n) = return n
  fromValue  _        = throwIO $ Expected "float-point number"

instance FromValue String where
  fromValue (Str n) = return n
  fromValue  _      = throwIO $ Expected "string"

instance FromValue Impl where
  fromValue (Fun n) = return n
  fromValue  _      = throwIO $ Expected "function"

instance FromValue (Map String Value) where
  fromValue (Obj n) = return n
  fromValue  _      = throwIO $ Expected "object"

class Embed x where
  embed :: x -> Impl

continue :: Embed x => x -> Function
continue = runImpl . embed

instance {-# OVERLAPS #-} ToValue x => Embed x where
  embed x = Native (show x) $ do
    stack %= (toValue x :)

instance (Embed a) => Embed (IO a) where
  embed io = Native "<IO>" $ do
    a <- lift io
    continue a

instance (Embed a, Embed b) => Embed (a, b) where
  embed (a, b) = Threaded [embed b, embed a]

data Underflow = Underflow
  deriving stock Show
  deriving anyclass Exception

pop :: FromValue a => StateT Machine IO a
pop = do
  s <- use stack
  case s of
    top : rest -> do
      a <- lift $ fromValue top
      stack .= rest
      return a

    _ -> do
      lift $ throwIO Underflow

instance (FromValue a, Embed b) => Embed (a -> b) where
  embed f = Native "<function>" $ do
    a <- pop
    let b = f a
    continue b

swap :: Impl
swap = embed impl
  where
    impl :: Value -> Value -> (Value, Value)
    impl a b = (b, a)

float :: Float -> Impl
float = embed

int :: Int -> Impl
int = embed

str :: String -> Impl
str = embed

native :: String -> Function -> Impl
native = Native

fun :: Impl -> Impl
fun = embed

empty :: Machine
empty = Machine
  [ ("swap", Entry  swap     False)
  , ("find", Entry  find     False)
  , ("step", Entry  step     False)
  , ("and",  Entry  andB     False)
  , ("if",   Entry  iff      False)
  , ("$",    Entry  inceptionDepth False)
  , (",",    Entry  compile False)
  , ("!",    Entry  evaluate False)
  , ("[",    Entry  quote True)
  , ("]",    Entry  exit  True)
  , ("forget", Entry forget False)
  ]
  []
  0
  "foo bar qux"

term :: Impl
term =
  native "term" do
    tape %= dropWhile isSpace
    t <- use tape
    let (term, rest) = break isSpace t
    tape .= rest
    continue term

data NotFound = NotFound String
  deriving stock Show
  deriving anyclass Exception

find :: Impl
find =
  native "find" $ do
    name <- pop
    d    <- use dict
    case lookup name d of
      Just (Entry fun immediate) ->
        continue (immediate, fun)

      Nothing -> do
        lift $ throwIO (NotFound name)

forget :: Impl
forget =
  native "forget" $ do
    name <- pop
    dict %= drop 1 . dropWhile ((name /=) . fst)

step :: Impl
step =
  Threaded
    [ inceptionDepth
    , int 0
    , equal @Int
    , andB
    , fun evaluate
    , fun compile
    , iff
    ]

equal :: forall a. (FromValue a, Eq a) => Impl
equal = embed $ (==) @a

andB :: Impl
andB = embed (&&)

iff :: Impl
iff =
  native "iff" $ do
    no   <- pop
    yes  <- pop
    bool <- pop
    if bool
    then do
      runImpl yes
    else do
      runImpl no

inceptionDepth :: Impl
inceptionDepth =
  native "$" $ do
    inc <- use depth
    continue inc

compile :: Impl
compile =
  native "compile" $ do
    impl <- pop
    Entry body imm <- assert EmptyDict $ preuse (dict._head._2)
    dict._head._2 .= Entry (body <> impl) imm

data EmptyDict = EmptyDict
  deriving stock Show
  deriving anyclass Exception

assert :: Exception e => e -> StateT Machine IO (Maybe a) -> StateT Machine IO a
assert e action = do
  ma <- action
  maybe (lift $ throwIO e) return ma

evaluate :: Impl
evaluate =
  native "evaluate" $ do
    impl <- pop
    runImpl impl

quote :: Impl
quote =
  native "<[" $ do
    depth .= 1

exit :: Impl
exit =
  native "]>" $ do
    depth .= 0