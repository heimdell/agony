
import Polysemy
import Polysemy.Error
import Polysemy.State
import Polysemy.Reader

import Program
import Value
import Constant
import Eval (evaluate)
import Errors
import Effect.HasScope
import Effect.Builtins
import Effect.Access
import Effect.HasStack

import qualified Scope
import qualified Stack
import qualified Name

main :: IO ()
main = do
  let
    -- test program
    prog =
      Let
        (Scope.fromAssoc
          [(#sqr, #dup `Seq` #mult)])
      (     3
      `Seq` 4
      `Seq` 5
      `Seq` 6
      `Seq` (#swap `Par` #swap)
      `Seq` (#id   `Par` #mult)
      )

  -- applying services (in inverse order)
  (stack, err) <- runM
    $ runReader Scope.empty
    $ runState  Stack.empty
    $ evalState (1 :: Int)
    $ runError
    $ mapError @NotANumber     (Right . Right . Right . Right)
    $ mapError @NotAnObject    (Right . Right . Right . Left)
    $ mapError @NotFound       (Right . Right . Left)
    $ mapError @StackUnderflow (Right . Left)
    $ mapError @Undefined       Left
    $ runStack
    $ runAccess
    $ runBuiltins stdlib
    $ runScope
    $ evaluate prog

  case err of
    Right () -> do
      print (stack :: Stack)

    Left e -> do
      print e

  return ()

  where
    -- standard library
    stdlib
      :: Member HasStack r
      => Member (Error Undefined) r
      => Member (Error NotANumber) r
      => Name.T
      -> Sem r ()
    stdlib name@(Name.raw -> raw) =
      case raw of
        "dup" -> do
          x <- pop
          push x
          push x

        "swap" -> do
          x <- pop
          y <- pop
          push x
          push y

        "id" -> do
          x <- pop
          push x

        "mult" -> do
          x <- pop
          y <- pop
          case (x, y) of
            (Val (Num a), Val (Num b)) -> do
              push (Val (Num (a * b)))

            _ -> do
              throw NotANumber

        _ -> do
          throw (Undefined name)
